﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MusicManager
{
    class Album
    {
        virtual public int ID { set; get; }
        virtual public string Name { set; get; }
        virtual public Artist Artist { set; get; }
        virtual public IList<Track> Tracks { set; get; }

        public Album()
        {
            Tracks = new List<Track>();
        }

        public Album(string name)
        {
            Tracks = new List<Track>();
            Name = name;
        }
    }
}