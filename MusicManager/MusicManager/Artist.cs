﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MusicManager
{
    class Artist
    {
        virtual public int ID { set; get; }
        virtual public String Name { set; get; }
        virtual public IList<Album> Alnums { set; get; }

        public Artist()
        {
            Alnums = new List<Album>();
        }

        public Artist(string name)
        {
            Alnums = new List<Album>();
            Name = name;
        }
    }


}
