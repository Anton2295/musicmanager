﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MusicManager
{
    class Genre
    {
        virtual public int ID { set; get; }
        virtual public string Name { set; get; }

        public Genre()
        {
        }

        public Genre(string name)
        {
            Name = name;
        }
    }
}
