﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace MusicManager.Mappings
{
    class AlbumMap : ClassMap<Album>
    {
        public AlbumMap()
        {
            Table("albom");
            Id(x => x.ID)
                .GeneratedBy.Native();
            Map(x => x.Name);
            References(x => x.Artist);

            HasManyToMany(x => x.Tracks)
                .Table("artistOnTtack")
                .Cascade.SaveUpdate();
        }
    }
}
