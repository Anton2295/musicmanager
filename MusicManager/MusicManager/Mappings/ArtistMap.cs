﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace MusicManager
{
    class ArtistMap : ClassMap<Artist>
    {
        public ArtistMap()
        {
            Table("artist");
            Id(x => x.ID)
                .GeneratedBy.Native();
            Map(x => x.Name);

        }
    }
}

