﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace MusicManager.Mappings
{
    class GenreMap : ClassMap<Genre>
    {
        public GenreMap()
        {
            Table("genr");
            Id(x => x.ID)
                .GeneratedBy.Native();
            Map(x => x.Name);
        }
    }
}
