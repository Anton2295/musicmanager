﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System.Reflection;

namespace MusicManager
{
    class Program
    {
        const string NULL_ALBUM = "без альбома";
        const int SECONDS_IN_A_MINUTE = 60;

        static void Main(string[] args)
        {


            FluentConfiguration config = Fluently
                .Configure()
                .Database(MsSqlConfiguration
                              .MsSql2008
                              .ConnectionString(@"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\Users\Антон\Desktop\Курсы\Часть5\MusicManager\MusicManager\Database1.mdf;Integrated Security=True;User Instance=True"))
                .Mappings(configuration => configuration.FluentMappings.AddFromAssembly(Assembly.GetEntryAssembly()));

            var export = new SchemaUpdate(config.BuildConfiguration());
            export.Execute(true, true);

            ISessionFactory sessionFactory = config.BuildSessionFactory();

            using (ISession session = sessionFactory.OpenSession())
            {


                using (var transaction = session.BeginTransaction())
                {
                    List<TrackOnText> tracksOnText = ReadSongs("music.txt");

                    foreach (TrackOnText trackOnText in tracksOnText)
                    {
                        List<String> genresNames = trackOnText.Genres;
                        List<Genre> genres = new List<Genre>();

                        foreach (string genrName in genresNames)
                        {
                            Genre genre = session.QueryOver<Genre>()
                                .Where(x => x.Name == genrName)
                                .List()
                                .FirstOrDefault();

                            if (genre == null)
                            {
                                genre = new Genre();
                            }

                            genre.Name = genrName;                        
                            session.Save(genre);
                            genres.Add(genre);
                        }


                        string artistName = trackOnText.ArtistName;
                        Artist artist = session.QueryOver<Artist>()
                            .Where(x => x.Name == artistName)
                            .List()
                            .FirstOrDefault();

                        if (artist == null)
                        {
                            artist = new Artist();
                        }

                        artist.Name = artistName;                    
                        session.Save(artist);


                        string albumName = trackOnText.AlbumName;
                        if (albumName == "")
                            albumName = NULL_ALBUM;

                        Album album = session.QueryOver<Album>()
                            .Where(x => x.Name == albumName)
                            .List()
                            .FirstOrDefault();

                        if (album == null)
                        {
                            album = new Album();
                        }

                        album.Name = albumName;
                        artist.Alnums.Add(album);
                        album.Artist = artist;
                        session.Save(album);


                        string trackName = trackOnText.Name;
                        Track track = session.QueryOver<Track>()
                            .Where(x => x.Name == trackName)
                            .List()
                            .FirstOrDefault();

                        if (track == null)
                        {
                            track = new Track();
                            track.Genres = genres;
                            track.Length = trackOnText.Time;
                        }

                        track.Name = trackName;

                        album.Tracks.Add(track);
                        session.Save(track);

                        Console.WriteLine("Все альбомы указанной группы.");
                        Console.WriteLine("Все песни из всех альбомов указанной группы.");
                        Console.WriteLine("Все пары групп, у которых есть одноименные песни");
                        Console.WriteLine("Все песни, указанного жанра..");
                        Console.WriteLine("Все песни, являющиеся одновременно песнями нескольких указанных жанров.");
                        Console.WriteLine("Все песни, являющиеся одним из указанных жанров.");
                    }

                    transaction.Commit();
                }
            }
        }

        static List<TrackOnText> ReadSongs(String fileName)
        {
            List<TrackOnText> tracks = new List<TrackOnText>();

            string text = null;
            using (StreamReader sr = new StreamReader(fileName))
            {
                text = sr.ReadToEnd();
            }
            Console.WriteLine(text);

            String namberRegex = @"(?<число>[\d]+)";
            String nameRegex = @"(?<Имя>[\w\s]+)";
            String performerRegex = @"(?<Исполнитель>[\w\s]+)";
            String albumRegex = @"\(?(?<Альбом>[\w\s]*)\)?";
            String timeRegex = @"(?<минуты>[0-9\s]+)" + ":" + @"(?<секунды>[0-9\s]+)";
            String genresRegex = @"(?<Жанры>[\w\s,-]+)";

            Regex regex = new Regex(namberRegex + "." + nameRegex + "-" + performerRegex + albumRegex + ";" + timeRegex + ";" + genresRegex + "\n");
            MatchCollection matches = regex.Matches(text);
            foreach (Match match in matches)
            {
                int namber = Convert.ToInt16(match.Groups["число"].Value);
                string name = match.Groups["Имя"].Value;
                string artistName = match.Groups["Исполнитель"].Value;
                string albumName = match.Groups["Альбом"].Value;
                int minute = Convert.ToInt16(match.Groups["минуты"].Value);
                int second = Convert.ToInt16(match.Groups["секунды"].Value);
                List<string> genres = match.Groups["Жанры"].Value.Split(',').ToList();
                Console.WriteLine(namber + name + artistName + albumName + minute.ToString() + ":" + second.ToString() + genres[0] + "\n");

                TrackOnText trackOnText = new TrackOnText()
                {
                    Name = name,
                    ArtistName = artistName,
                    AlbumName = albumName,
                    Time = second + SECONDS_IN_A_MINUTE * minute,
                    Genres = genres
                };


            }
            return tracks;
        }
    }
}
