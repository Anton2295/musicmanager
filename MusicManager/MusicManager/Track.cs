﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MusicManager
{
    class Track
    {
        virtual public int ID { set; get; }
        virtual public string Name { set; get; }
        virtual public int Length { set; get; }
        virtual public IList<Album> Albums { set; get; }
        virtual public IList<Genre> Genres { set; get; }

        public Track()
        {
            Albums = new List<Album>();
            Genres = new List<Genre>();
        }

    }
}
