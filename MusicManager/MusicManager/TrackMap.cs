﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace MusicManager.Mappings
{
    class TrackMap : ClassMap<Track>
    {
        public TrackMap()
        {
            Table("track");
            Id(x => x.ID)
                .GeneratedBy.Native();
            Map(x => x.Name);
            Map(x => x.Length);

            HasManyToMany(x => x.Albums)
              .Table("artistOnTtack")
              .Cascade.SaveUpdate();

            HasManyToMany(x => x.Genres)
                .Table("artistOnGenres")
                .Cascade.SaveUpdate();
        }
    }
}
