﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MusicManager
{
    class TrackOnText
    {
        public int Namber { set; get; }
        public string Name { set; get; }
        public string ArtistName { set; get; }
        public string AlbumName { set; get; }
        public int Time { set; get; }
        public List<string> Genres { set; get; }

        public TrackOnText()
        {
            Genres = new List<string>();
        }
    }
}
